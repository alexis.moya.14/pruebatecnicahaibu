const clients = [
    { id: 1, rut: "89873550", name: "LARA RENE PETTY BERGER" },
    { id: 2, rut: "86833361", name: "CONWAY LANDRY POLLARD BRADLEY" },
    { id: 3, rut: "88271452", name: "MICHELLE LETITIA BATTLE MOONEY" },
    { id: 4, rut: "87252013", name: "SIMMONS NELSON WITT MONROE" },
    { id: 5, rut: "81706494", name: "BRADY MARY RANDALL FERNANDEZ" },
    { id: 6, rut: "71167355", name: "ACOSTA COLE ATKINSON PITTS" },
    { id: 7, rut: "79093176", name: "DOMINGUEZ HOUSE GONZALES SALAZAR" }, { id: 8, rut: "91361017", name: "KIRSTEN COLLINS BYERS COFFEY" },
    { id: 9, rut: "9065642K", name: "FIELDS RATLIFF MORRIS QUINN" }
    ];

const banks = [
    { id: 1, name: 'SCOTIABANK' },
    { id: 2, name: 'BCI' },
    { id: 3, name: 'ITAU' },
    { id: 4, name: 'CONDELL' },
    ];

const accounts = [
    { clientId: 4, bankId: 1, balance: 79069 }, 
    { clientId: 6, bankId: 3, balance: 136060 }, 
    { clientId: 9, bankId: 3, balance: 74908 },
    { clientId: 2, bankId: 2, balance: 4391 },
    { clientId: 6, bankId: 2, balance: 116707 },
    { clientId: 1, bankId: 3, balance: 157627 },
    { clientId: 5, bankId: 4, balance: 136372 },
    { clientId: 7, bankId: 4, balance: 190204 },
    { clientId: 5, bankId: 4, balance: 149670 },
    { clientId: 2, bankId: 3, balance: 143321 },
    { clientId: 2, bankId: 4, balance: 67466 },
    { clientId: 2, bankId: 3, balance: 17956 },
    { clientId: 9, bankId: 2, balance: 43194 },
    { clientId: 5, bankId: 1, balance: 52245 },
    { clientId: 6, bankId: 2, balance: 41562 },
    { clientId: 3, bankId: 2, balance: 138046 },
    { clientId: 6, bankId: 3, balance: 196964 },
    { clientId: 8, bankId: 3, balance: 73803 },
    { clientId: 9, bankId: 2, balance: 150402 },
    { clientId: 7, bankId: 1, balance: 122869 },
    { clientId: 5, bankId: 4, balance: 65223 },
    { clientId: 7, bankId: 3, balance: 143589 },
    { clientId: 9, bankId: 3, balance: 43346 },
    { clientId: 2, bankId: 1, balance: 60421 },
    { clientId: 4, bankId: 4, balance: 184110 },
    { clientId: 8, bankId: 4, balance: 195903 },
    { clientId: 5, bankId: 2, balance: 77649 },
    { clientId: 8, bankId: 4, balance: 28170 },
    { clientId: 5, bankId: 1, balance: 132850 },
    { clientId: 1, bankId: 3, balance: 139679 },
    { clientId: 7, bankId: 4, balance: 119808 },
    { clientId: 4, bankId: 4, balance: 109201 },
    { clientId: 9, bankId: 3, balance: 112529 },
    { clientId: 1, bankId: 3, balance: 137914 },
    { clientId: 6, bankId: 2, balance: 122904 },
    { clientId: 5, bankId: 4, balance: 103142 },
    { clientId: 8, bankId: 2, balance: 69163 },
    { clientId: 2, bankId: 4, balance: 57812 },
    { clientId: 2, bankId: 3, balance: 32851 },
    { clientId: 7, bankId: 1, balance: 109763 },
    { clientId: 8, bankId: 3, balance: 147442 },
    { clientId: 9, bankId: 1, balance: 42217 },
    { clientId: 1, bankId: 1, balance: 39658 },
    { clientId: 6, bankId: 2, balance: 8664 },
    { clientId: 8, bankId: 1, balance: 41915 },
    { clientId: 7, bankId: 1, balance: 31879 },
    { clientId: 7, bankId: 4, balance: 117795 },
    { clientId: 1, bankId: 4, balance: 108862 },
    { clientId: 5, bankId: 1, balance: 18550 },
];

function exercise0() {
    return clients.map(client =>client.id);
 }

/**
Ejercicio 1
@description Retornar un arreglo con los ID de los clientes ordenados por RUT 
*/
function exercise1() {
    let clientesAux = clients.sort(function(c1, c2) {
        return c1.rut - c2.rut;
    });
    return clientesAux.map(client =>client.id);
}

/**
Ejercicio 2
@description Retornar un arreglo con los nombres de los clientes, ordenados de mayor a menor por la suma TOTAL de los saldos de las Cuentas
*/
function exercise2() {
    //generar arreglo de clientes agregandole su saldo consolidado
    arrClienteSaldo=[]
    clients.map(client =>{
        totalCliente=0;
         let cuentas = accounts.filter(function (e) {
            return e.clientId==client.id;
         });
         cuentas.map(c =>{
            totalCliente+=c.balance
         });
         client.balance = totalCliente
         arrClienteSaldo.push(client)
    });
   
    //una vez tengo el arreglo con los saldos los retorno ordenados
    arrClienteSaldo.sort(function(c1, c2) {
        return c2.balance - c1.balance;
    });

    return arrClienteSaldo.map(client =>{
        return {'nombre':client.name,'balance':client.balance}
    }); 
}

/**
Ejercicio 3
@description Devuelve un objeto cuyo índice es el nombre de los bancos
y cuyo valor es un arreglo de los ruts de los clientes ordenados alfabéticamente por 'nombre' */
function exercise3() {
      //generar arreglo de clientes agregandole su saldo consolidado
      bankAux=[]; // ocupare un arr aux para validar datos
      let clientesAux = clients.sort(function (a, b) {
        if (a.name > b.name) {
          return 1;
        }
        if (a.name < b.name) {
          return -1;
        }
        return 0;
      });
      clientesAux.map(client =>{
          totalCliente=0;
           let cuentas = accounts.filter(function (e) {
              return e.clientId==client.id;
           });
           cuentas.map(c=>{
                let bancos = banks.filter(function (b) {
                return b.id==c.bankId;
                });
                //validacion
                if (bankAux.length==0){// si esta vacio ingreso el primer elemento
                    bankAux.push({'banco':bancos[0].name,clientes:[client.rut] })
                }else{
                    //si existe un elemento o mas recorro mi lista  y pregunto si exsite el banco
                    let existeBanco = false;
                    bankAux.forEach(bancoAux => {
                        if (bancoAux.banco==bancos[0].name){
                            existeBanco=true;
                            //si el banco existe agrego a la persona en el caso de no tenerla en mi arreglo
                            let existeCliente=false
                            bancoAux.clientes.forEach(e => {
                               if (e==client.rut){
                                existeCliente=true
                               }
                            });
                            if (!existeCliente){
                                bancoAux.clientes.push(client.rut)
                            }
                        }
                    });
                    if (!existeBanco){
                        bankAux.push({'banco':bancos[0].name,clientes:[client.rut] })
                    }
                }
            
           })
      });
      return bankAux;
}

/**
Ejercicio 4
@description Devuelve un arreglo ordenado de mayor a menor con el saldo de los clientes que tengan más de 25000 en el banco 'SCOTIABANK'
*/
function exercise4() {
    bancoBuscado='SCOTIABANK'
    let banco = banks.filter(function (e) {
        return e.name==bancoBuscado;
     });
     //puse en una lista solo las cuentas del banco buscado y posteriormente la voy a asociar con el cliente
     let cuentasBanco = accounts.filter(function (cuentas) {
        return cuentas.bankId==banco[0].id;
     });
     let arrAux=[] //generare un arreglo donde sume los balances de cada cliente y posterior retornare los mayores a 25000
     cuentasBanco.forEach(element => {
         let existeCliente=false;
         arrAux.forEach(e => {
             if(element.clientId==e.clientId){
                existeCliente=true;
                e.balance+=element.balance;
             }  
         });
         if(!existeCliente){
            let cliente = clients.filter(function (c) {
                return c.id==element.clientId;
             });
            arrAux.push({'clientId':element.clientId,'balance':element.balance,nombre:cliente[0].name})
         }
     });
     //dejo solo los mayores a 25.000
     let saldosMayores = arrAux.filter(function (e) {
        return e.balance>25000;
     });
     saldosMayores.sort(function(c1, c2) {
        return c2.balance - c1.balance;
    });
    return saldosMayores;
     

}

/**
Ejercicio 5
@description Devuelve un arreglo con la 'id' de los Bancos de menor a mayor por el TOTAL de dinero que administran en las cuentas de sus clientes
*/
function exercise5() {
     arrAux=[];
     banks.forEach(banco => {
        let cuentasBanco = accounts.filter(function (cuentas) {
            return cuentas.bankId==banco.id;
        });
        let TotalBanco=0;
        cuentasBanco.forEach(cta => {
            TotalBanco+=cta.balance;
        });
        arrAux.push({'banco':banco.name,'total':TotalBanco})
        
     });

     arrAux.sort(function(c1, c2) {
        return c2.total - c1.total;
    });
    return arrAux;
     
}

/**
Ejercicio 6
@description Devuelve un objeto en donde la key son los nombre de los bancos y el valor es la cantidad de clientes que solo tienen una cuenta en ese banco
*/
function exercise6() {
    arrAux=[];
    banks.forEach(banco => {
       let cuentasBanco = accounts.filter(function (cuentas) {
           return cuentas.bankId==banco.id;
       });
       let cuentasUnicas = {'banco':banco.name,'cantidad':0}
       cuentasBanco.forEach(element => {
            let cuentasBancoMismoCliente = cuentasBanco.filter(function (cuenta) { //creo un arr con las cuentas de 1 cliente en 1 banco
            return cuenta.clientId==element.clientId;
            });
            cuentasBancoMismoCliente.length==1?cuentasUnicas['cantidad']+=1:cuentasUnicas['cantidad']
       });

       arrAux.push(cuentasUnicas)
       
    });
    return arrAux;
}

/**
Ejercicio 7
@description Devuelve un objeto en donde la key son el nombre de los bancos y el valor es el 'id' de su cliente con menos dinero.
*/
function exercise7() {
    arrMenorDinero=[]
    banks.forEach(banco => {
        let cuentasBanco = accounts.filter(function (cuentas) {
        return cuentas.bankId==banco.id;
        });
        let arrAux=[] //generare un arreglo donde sume los balances de cada cliente
        cuentasBanco.forEach(element => {
            let existeCliente=false;
            arrAux.forEach(e => {
                if(element.clientId==e.clientId){
                existeCliente=true;
                e.balance+=element.balance;
                }  
            });
            if(!existeCliente){
            let cliente = clients.filter(function (c) {
                return c.id==element.clientId;
                });
            arrAux.push({'clientId':element.clientId,'balance':element.balance,nombre:cliente[0].name})
            }
        });
        arrAux.sort(function(c1, c2) {
            return c1.balance - c2.balance;
        });
        arrMenorDinero.push({banco:banco.name,clienteId:arrAux[0].clientId,nombre:arrAux[0].nombre})
       
     
    });

    return arrMenorDinero;
}

console.log("Ejercicio 0 --> ", exercise0() || "Ejercicio no resuelto");
console.log("Ejercicio 1 --> ", exercise1() || "Ejercicio no resuelto");
console.log("Ejercicio 2 --> ", exercise2() || "Ejercicio no resuelto");
console.log("Ejercicio 3 --> ", exercise3() || "Ejercicio no resuelto");
console.log("Ejercicio 4 --> ", exercise4() || "Ejercicio no resuelto");
console.log("Ejercicio 5 --> ", exercise5() || "Ejercicio no resuelto");
console.log("Ejercicio 6 --> ", exercise6() || "Ejercicio no resuelto");
console.log("Ejercicio 7 --> ", exercise7() || "Ejercicio no resuelto"); 