import { Component, OnInit, Input } from '@angular/core';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { faCheckCircle, faTimesCircle } from '@fortawesome/free-solid-svg-icons';
@Component({
  selector: 'tarjeta',
  templateUrl: './tarjeta.component.html',
  styleUrls: ['./tarjeta.component.css']
})
export class TarjetaComponent implements OnInit {

  closeResult = '';
  faCheckCircle = faCheckCircle;
  faTimesCircle = faTimesCircle;

  constructor(private modalService: NgbModal) {}

  @Input() data;

  ngOnInit() {

  }

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  public ValidadorFecha(dato){
    let re = /^\d{1,2}\/\d{1,2}\/\d{4}$/;
    if(dato!= '' && !dato.match(re)) {
      return false;
    }
    return true;
  }


    // Valida el rut con su cadena completa "XXXXXXXX-X"
    public validaRut(rutCompleto) {
      if (!/^[0-9]+[-|‐]{1}[0-9kK]{1}$/.test( rutCompleto ))
        return false;
      var tmp 	= rutCompleto.split('-');
      var digv	= tmp[1]; 
      var rut 	= tmp[0];
      if ( digv == 'K' ) digv = 'k' ;
      return (this.dv(rut) == digv );
    }

   private dv(T){
      var M=0,S=1;
      for(;T;T=Math.floor(T/10))
        S=(S+T%10*(9-M++%6))%11;
      return S?S-1:'k';
    
  }

}
