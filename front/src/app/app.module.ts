import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { TarjetaComponent } from './components/tarjeta/tarjeta.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

@NgModule({
  declarations: [
    AppComponent,
    TarjetaComponent
    
  ],
  imports: [
    BrowserModule,
    FontAwesomeModule
  ], 
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
