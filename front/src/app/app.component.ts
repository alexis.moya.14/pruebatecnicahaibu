import { Component } from '@angular/core';
import axios from 'axios';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(){}
  public data=null;
  public data_aux=null;
  async ngOnInit(){
    console.log('funcionando onInit')
    let data =  await axios.get('https://my-json-server.typicode.com/HaibuSolutions/prueba-tecnica-sf/user');
    this.data=data.data;
  }




  onSearchChange(searchValue: string): void {

    if(searchValue==''){
      this.data=this.data_aux;
    }else{
      if(this.data_aux==null){ //si es mi primera busqueda guardo el resultado principal para usarlo despues
        this.data_aux=this.data;
      }
      
    let dataFiltrada = this.data.filter(function (cuentas) {
        //console.log(cuentas.nombre, cuentas.apellido )
        //return []
        return cuentas.nombre.toLowerCase().includes(searchValue.toLowerCase());
    });

    this.data=dataFiltrada;
    }
  }
}
